require 'sinatra/base'
require 'sinatra/reloader' #if settings.development?
#require 'sinatra/flash'
require 'slim'
# require 'sass'
#require 'pony'
# require 'coffee-script'
# require 'v8'
# require 'sinatra/auth'
# require 'sinatra/contact'
# require 'asset-handler'

require './helpers/aplicacion_helper'

class AplicacionController < Sinatra::Base
 
  helpers AplicacionHelpers
 
  #enable :development

  #set :views, File.expand_path('../../views', __FILE__)
  #enable :sessions, :method_override
 
  #register Sinatra::Auth
  #register Sinatra::Contact
  # register Sinatra::Flash
 
  # use AssetHandler
 
  # not_found{ slim :not_found }
end