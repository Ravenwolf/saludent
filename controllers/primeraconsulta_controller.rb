#require './controllers/aplicacion_controller'
require './helpers/primeraconsulta_helper'

class PrimeraConsultaController < AplicacionController
  helpers PrimeraConsultaHelpers

  set :views, './views/primera'

  get '/' do
    "Primera consulta"
  end
end