require './helpers/preconsulta_helper'

class PreConsultaController < AplicacionController
  helpers PreConsultaHelpers

  set :views, './views/preconsulta'

  get '/' do
    slim :preconsulta
  end
end