#require './controllers/aplicacion_controller'
require './helpers/principal_helper'

class PrincipalController < AplicacionController
  helpers PrincipalHelpers
 
  get '/' do
    #slim :home
    "Principal"
  end
end