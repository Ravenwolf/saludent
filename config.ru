require 'rubygems'                                                                                                
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('Gemfile', __FILE__)                                                   
require 'bundler/setup' if File.exists?(ENV['BUNDLE_GEMFILE'])

#require 'sinatra/base'

#Dir.glob('./{models,helpers,controllers}/*.rb').each { |file| require file }
require './controllers/aplicacion_controller'
require './controllers/principal_controller'
require './controllers/primeraconsulta_controller'
require './controllers/preconsulta_controller'

#SongController.configure :development do
#  DataMapper.setup(:default, "sqlite3://#{Dir.pwd}/development.db")
#end
 
#SongController.configure :production do
#  DataMapper.setup(:default, ENV['DATABASE_URL'])
#end

map('/preconsulta') {run PreConsultaController}
map('/primera') { run PrimeraConsultaController }
map('/') { run PrincipalController }